The files were created using dhcpd and Kea daemons. Here's a bunch of commands
that were used to generate those:

Here's the network diagram:

```
srv1: 192.168.1.1 ------------------------- srv2: 192.168.1.2
```

- Generate v4 traffic (1 packet/sec, simulate 10 clients, send 15 transactions)
  ```perfdhcp -t1 -r 1 -R 10 -n 15 192.168.1.2```

- Generate v6 traffic (1 pkt/sec, simulate 10 clients, send 15 transactions)
  ```perfdhcp -6 -r 1 -R 10 -n 15 3ffe:501:ffff:100::ab:c```

- Generate v6 traffic (mix of renewals and new allocations, for both IA_NA and IA_PD)
  ```perfdhcp -6 -f 1 -r 2 -t 1 -R 10 -n 15 -l eth1 -e address-and-prefix```

- Run kea:
  ```kea-dhcp4 -c /etc/kea/kea-dhcp4.conf```

- Run dhcpd:
  ```dhcpd -4 -d -cf /etc/dhcp/dhcpd.conf```
  ```dhcpd -6 -d -cf /etc/dhcp/dhcpd6.conf```
