# Copyright (C) 2018-2023 Internet Systems Consortium, Inc. ("ISC")
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# dhcp2kea by marmo (17-12-2022, 23-12-2022, 30-12-2022)
### Package content
 
The package contains the following files in the form of source codes:

- [dhcp2kea.py](http://knot805.eti.pg.gda.pl/dhcp/dhcp2kea.py) - python3 converter
- [dhcp2kea.php](http://knot805.eti.pg.gda.pl/dhcp/dhcp2kea.html) - the driver for running the converter from a web-browser with simple user interface
- [dhcp2kea.html](http://knot805.eti.pg.gda.pl/dhcp/dhcp2kea.html) - web interface for testing the converter with syntax colored items

Additionally, there are three examples: one for dhcp4 and two for dhcp6:

1. [example1-dhcpd4.leases](http://knot805.eti.pg.gda.pl/dhcp/example1-dhcpd4.leases)
2. [example2-dhcpd6.leases](http://knot805.eti.pg.gda.pl/dhcp/example1-dhcpd4.leases)
3. [example3-dhcpd6.leases](http://knot805.eti.pg.gda.pl/dhcp/example1-dhcpd4.leases)

along with its converted equivalents. Their names ends with `-kea.csv`.

#### Note
At the moment of writing the package is located on the  author's www server [knot805.eti.pg.gda.pl](http://knot805.eti.pg.gda.pl/dhcp). 

### Using dhcp2kea converter
The converter is written int Python3. Its usage is described by the following string defined direcly in source file string:

```
Usage: python3 dhcp2kea.py ["[addr ]key=value"] [-e[dateTtime] [-v] [-h] [-d] [-t] leases-filename(s)
Options:
  -h : help - prints this help
  -v : verbose - print results to stdout
  -d : debug - print internal state
  "key=value" : default values for each lease
  "addr key=value" : default value for addr
  -e[dateTtime] : expire - overriding expire value with datetime (defaults to current)
  -t[n] : test - execute test/demo functions, optional n=1,2,3,4,6 (internal test no) defaults to all
Key names:
  uid, subnet_id, client-hostname, lease_type, prefix_len, 
  hwaddr, state, user_context, hwtype, hwaddr_source, 
  expire, valid_liftime
Examples:
  1.setting subnet_id=2 for all leases:
    python3 dhcp2kea.py "subnet_id=2" example1-dhcpd4.leases
  2.setting subnet_id=3 for 192.168.1.12 to 192.168.1.16 and subnet_id=2 for 192.168.1.20 to 192.168.1.29
    python3 dhcp2kea.py "192.168.1.1[2-6] subnet_id=3" "192.168.1.2\d subnet_id=2" example1-dhcpd4.leases
  3.setting subnet_id=3 for 192.168.1.12 and subnet_id=8 for 3ffe:501:ffff:100::c9d9 in two processed files
    python3 dhcp2kea.py "192.168.1.12 subnet_id=3" example1-dhcpd4.leases "3ffe:501:ffff:100::c9d9 subnet_id=8" example2-dhcpd6.leases
  4.setting expiration time to January,4th 2023 10 o'clock localtime for all leases
    python3 dhcp2kea.py -e2023/01/04T10:00:00 example1-dhcpd4.leases
  5.setting liftime of each lease starting from current time for internal test no 6 (dhcp6 string)
    python3 dhcp2kea.py "valid_liftime=3600" -e -t6
```

and is printed when calling the code without any parameters. The conversion could be performed for several files at ones but it is done sequentially. 
The -v option is used by web interface. 
The -t[n] option executes 5 tests:  three from accompaning examples (n=1,2,3) and two hardcoded in source code (n=4 and n=6).
When n is omitted all tests are performed. 
The "key=value" option alllows defining default values for each lease i.e.
```
c:\Apache24\htdocs\dhcp>python dhcp2kea.py -v "subnet_id=2" example1-dhcpd4.leases
example1-dhcpd4.leases:
address,hwaddr,client_id,valid_lifetime,expire,subnet_id,fqdn_fwd,fqdn_rev,hostname,state,user_context
192.168.1.10,08:00:27:b7:9a:17,ff:27:b7:9a:17:00:01:00:01:2b:23:56:24:08:00:27:31:08:c9,3600,1670429247,2,1,1,kea-srv2,0,
192.168.1.11,08:00:27:31:08:c9,ff:27:31:08:c9:00:01:00:01:2b:23:56:24:08:00:27:31:08:c9,3600,1670429248,2,1,1,kea-srv1,0,
192.168.1.12,00:0c:01:02:03:04,01:00:0c:01:02:03:04,300,1670426181,2,0,0,,0,
192.168.1.13,00:0c:01:02:03:05,01:00:0c:01:02:03:05,300,1670426182,2,0,0,,0,
192.168.1.14,00:0c:01:02:03:06,01:00:0c:01:02:03:06,300,1670426183,2,0,0,,0,
192.168.1.15,00:0c:01:02:03:07,01:00:0c:01:02:03:07,300,1670426184,2,0,0,,0,
192.168.1.16,00:0c:01:02:03:08,01:00:0c:01:02:03:08,300,1670426176,2,0,0,,0,
192.168.1.17,00:0c:01:02:03:09,01:00:0c:01:02:03:09,300,1670426177,2,0,0,,0,
192.168.1.18,00:0c:01:02:03:0a,01:00:0c:01:02:03:0a,300,1670426178,2,0,0,,0,
192.168.1.19,00:0c:01:02:03:0b,01:00:0c:01:02:03:0b,300,1670426179,2,0,0,,0,
192.168.1.20,00:0c:01:02:03:0c,01:00:0c:01:02:03:0c,300,1670426180,2,0,0,,0,
192.168.1.21,00:0c:01:02:03:0d,01:00:0c:01:02:03:0d,300,1670426181,2,0,0,,0,

    processed: 26     written: 12
```
wheres "addr key=value" overrides key/value pair for specified address or a range of addresses expresed in a form o regular expression i.e.
```
example1-dhcpd4.leases:
c:\Apache24\htdocs\dhcp>python dhcp2kea.py -v "192.168.1.1[2-6] subnet_id=3" "192.168.1.2\d subnet_id=2" example1-dhcpd4.leases
address,hwaddr,client_id,valid_lifetime,expire,subnet_id,fqdn_fwd,fqdn_rev,hostname,state,user_context
192.168.1.10,08:00:27:b7:9a:17,ff:27:b7:9a:17:00:01:00:01:2b:23:56:24:08:00:27:31:08:c9,3600,1670429247,1,1,1,kea-srv2,0,
192.168.1.11,08:00:27:31:08:c9,ff:27:31:08:c9:00:01:00:01:2b:23:56:24:08:00:27:31:08:c9,3600,1670429248,1,1,1,kea-srv1,0,
192.168.1.12,00:0c:01:02:03:04,01:00:0c:01:02:03:04,300,1670426181,3,0,0,,0,
192.168.1.13,00:0c:01:02:03:05,01:00:0c:01:02:03:05,300,1670426182,3,0,0,,0,
192.168.1.14,00:0c:01:02:03:06,01:00:0c:01:02:03:06,300,1670426183,3,0,0,,0,
192.168.1.15,00:0c:01:02:03:07,01:00:0c:01:02:03:07,300,1670426184,3,0,0,,0,
192.168.1.16,00:0c:01:02:03:08,01:00:0c:01:02:03:08,300,1670426176,3,0,0,,0,
192.168.1.17,00:0c:01:02:03:09,01:00:0c:01:02:03:09,300,1670426177,1,0,0,,0,
192.168.1.18,00:0c:01:02:03:0a,01:00:0c:01:02:03:0a,300,1670426178,1,0,0,,0,
192.168.1.19,00:0c:01:02:03:0b,01:00:0c:01:02:03:0b,300,1670426179,1,0,0,,0,
192.168.1.20,00:0c:01:02:03:0c,01:00:0c:01:02:03:0c,300,1670426180,2,0,0,,0,
192.168.1.21,00:0c:01:02:03:0d,01:00:0c:01:02:03:0d,300,1670426181,2,0,0,,0,

    processed: 26     written: 12
```
defines subnet_id=3 for addresses 192.168.1.12 to 192.168.1.16 and subnet_id=2 for 192.168.1.20 to 192.168.1.29.
The default value for subnet_id is set up internally to be equal 1.

The -e option allows defining expire value in the form of datetime string using "%Y/%m/%dT%H:%M:%S" fixed format. 
Note that the date and time fields are T-character separated what guaratees passing this value as one argument. 
When datetime value is ommited the current time is used i.e.

```
c:\Apache24\htdocs\dhcp>python dhcp2kea.py "valid_lifetime=3600" -t4
dhcpd leases conversion tests:
(4)
address,hwaddr,client_id,valid_lifetime,expire,subnet_id,fqdn_fwd,fqdn_rev,hostname,state,user_context
175.16.1.100,08:00:27:ca:dd:fc,,3600,1610381156,1,1,1,cserver,0,
175.16.1.101,08:00:27:80:3c:9d,,3600,1610381157,1,1,1,cclient,0,

processed: 4 written: 2

c:\Apache24\htdocs\dhcp>python dhcp2kea.py "valid_lifetime=3600" -e -t4
dhcpd leases conversion tests:
(4)
address,hwaddr,client_id,valid_lifetime,expire,subnet_id,fqdn_fwd,fqdn_rev,hostname,state,user_context
175.16.1.100,08:00:27:ca:dd:fc,,3600,1672830231,1,1,1,cserver,0,
175.16.1.101,08:00:27:80:3c:9d,,3600,1672830231,1,1,1,cclient,0,

processed: 4 written: 2

c:\Apache24\htdocs\dhcp>python dhcp2kea.py "valid_lifetime=7200" -e2023/01/04T10:00:00 -t4
dhcpd leases conversion tests:
(4)
address,hwaddr,client_id,valid_lifetime,expire,subnet_id,fqdn_fwd,fqdn_rev,hostname,state,user_context
175.16.1.100,08:00:27:ca:dd:fc,,7200,1672822800,1,1,1,cserver,0,
175.16.1.101,08:00:27:80:3c:9d,,7200,1672822800,1,1,1,cclient,0,

processed: 4 written: 2
```

The python code is based on one class Leases and its functionality is explained in Leases doc string in source file. The options are processes in source code main loop.

### Using web interface

The web interface allows using conversion tool from any browser provided the project files are located at the php scripting enabled webserver. If offers:

- editing lease data in a html panel
- processing panel data
- processing local files
- processing three server located examples
- saving processed data in csv file
- tabulated preview of processed data

The left panel has colored syntax showing items that are used in conversion process. A sample sceen shot is presented in Fig.1.
 
[![](http://knot805.eti.pg.gda.pl/dhcp/dhcp2kea.png)](http://knot805.eti.pg.gda.pl/dhcp/)
*Fig.1 Screendump showing web interface to dhcp2kea tool.*

### Programmer minimum working example

```python
import dhcp2kea

s=r"""
authoring-byte-order little-endian;
server-duid 00:01:00:01:28:f8:2c:53:08:00:27:c7:c6:20;
ia-na cc:bb:aa:00:00:03:00:01:31:32:33:34:35:36 {
  cltt 2 2021/10/12 11:22:10;
  iaaddr 3002::200 {
    binding state active;
    preferred-life 225;
    max-life 360;
    ends 2 2021/10/12 11:28:10;
    set ddns-fwd-name = "toms.four.example.com";
    set ddns-dhcid = "\000\002\001'\223`\344\240\350\230:[\264\220va3p-\335R\253\233\215\354\315%\346\034_\351\206k\002\224";
    set ddns-rev-name = "0.0.2.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.2.0.0.3.ip6.arpa.";
  }
}
"""

print(dhcp2kea.Leases(s))

"""
output:
address,duid,valid_lifetime,expire,subnet_id,pref_lifetime,lease_type,iaid,prefix_len,fqdn_fwd,fqdn_rev,hostname,hwaddr,state,user_context,hwtype,hwaddr_source
3002::200,cc:bb:aa:00:00:03:00:01:31:32:33:34:35:36,360,1634030890,1,225,,,,1,1,toms.four.example.com,,0,,,
"""
```
 
