The keama-leases project was merged with the keama.

See `leases` directory in https://gitlab.isc.org/isc-projects/keama/.

All open tickets were moved.